import {spawn} from "child_process";

interface UpgraderMemory extends CreepMemory {
  [key: string]: any;
}

export class Upgrader extends Creep {
  private currentConstructionSite: (ConstructionSite | null) = null;
  private miners: Creep[] = [];
  memory: UpgraderMemory;

  /* @param {Creep} creep **/
  constructor(props: Creep) {
    super(props.id);
    this.memory = super.memory;
    this.miners = _.filter(Game.creeps, (creep) => creep.memory.role === 'miner');
  }

  public run() {
    this.currentConstructionSite = this.pos.findClosestByRange(FIND_CONSTRUCTION_SITES) || null;
    this.memory.construct = !!this.currentConstructionSite;

    if (this.miners.length && this.miners[0].pos.x !== 12 && this.miners[0].pos.y !== 19) { // move creeps away from energy source while there's no miner
      this.moveTo(32, 12);
      return;
    }

    if(this.memory.upgrading && this.store.getUsedCapacity() === 0) {
      this.memory.upgrading = false;
      this.say('🔄 harvest');
    }

    if(!this.memory.upgrading && this.store.getFreeCapacity() === 0) {
      this.memory.upgrading = true;
      this.say('⚡ upgrade');
    }
    if(this.memory.upgrading) {
      const spawnsAndExtensions = this.getSpawnsAndExtensions();
      const towers = this.getTowers();

      if(spawnsAndExtensions.length > 0 && this.memory.role === 'spawnFeeder') {
        if(this.transfer(spawnsAndExtensions[0], RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
          this.moveTo(spawnsAndExtensions[0], {visualizePathStyle: {stroke: '#ffffff'}});
        }
      } else if(towers.length > 0 && this.memory.role === 'towerFeeder') {
        if(this.transfer(towers[0], RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
          this.moveTo(towers[0], {visualizePathStyle: {stroke: '#ffffff'}});
        }
      } else if(this.memory.construct) {
        if(this.currentConstructionSite && this.build(this.currentConstructionSite) === ERR_NOT_IN_RANGE) {
          this.moveTo(this.currentConstructionSite)
        }
      } else if(this.upgradeController(Game.rooms['E5S12'].controller!) === ERR_NOT_IN_RANGE) {
        this.moveTo(Game.rooms['E5S12'].controller!, {visualizePathStyle: {stroke: '#ffffff'}});
      }

    }
    else {
      const droppedResources = Game.rooms['E5S12'].find(FIND_DROPPED_RESOURCES);
      const containers = Game.rooms['E5S12'].find(FIND_STRUCTURES, {
        filter: (s: any) => s.structureType === STRUCTURE_CONTAINER && s.store.getUsedCapacity() > 0
      });

      if(droppedResources && droppedResources[0].amount > 50 && this.pickup(droppedResources[0]) === ERR_NOT_IN_RANGE) {
        this.moveTo(droppedResources[0], {visualizePathStyle: {stroke: '#ffaa00'}});
      } else if(containers && this.withdraw(containers[0], RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
        this.moveTo(containers[0], {visualizePathStyle: {stroke: '#ffaa00'}});
      } else if(!this.miners.length && this.memory.role === 'spawnFeeder') {
        const sources = Game.rooms['E5S12'].find(FIND_SOURCES);
        if(this.harvest(sources[0]) === ERR_NOT_IN_RANGE) {
          this.moveTo(sources[0], {visualizePathStyle: {stroke: '#ffaa00'}});
        }
      }
    }
  }

  private getSpawnsAndExtensions(): (StructureExtension|StructureSpawn)[] {
    return Game.rooms['E5S12'].find(FIND_STRUCTURES, {
      filter: (s: StructureExtension|StructureSpawn) => {
        return ['extension', 'spawn'].includes(s.structureType) && (s.store.getFreeCapacity(RESOURCE_ENERGY) > 0);
      }
    }) as (StructureExtension|StructureSpawn)[];
  }

  private getTowers(): StructureTower[] {
    return Game.rooms['E5S12'].find(FIND_STRUCTURES, {
      filter: (structure: any) => {
        return structure.structureType === STRUCTURE_TOWER &&
          (structure.store.getUsedCapacity(RESOURCE_ENERGY) / structure.store.getCapacity(RESOURCE_ENERGY) < 0.5);
      }
    }) as (StructureTower)[];
  }

}
