const roleMiner = {

  /* @param {Creep} creep **/
  run(creep: any) {
    const sources = creep.room.find(FIND_SOURCES);
    const miners = _.filter(Game.creeps, (creep) => creep.memory.role === 'miner');

    if(miners.length === 1) {
      if(creep.harvest(sources[0]) === ERR_NOT_IN_RANGE) {
        creep.moveTo(sources[0], {visualizePathStyle: {stroke: '#ffaa00'}});
      }
    } else if (miners.length === 2 && miners.indexOf(creep) > 0) {
      if(creep.harvest(sources[1]) === ERR_NOT_IN_RANGE) {
        creep.moveTo(sources[1], {visualizePathStyle: {stroke: '#ffaa00'}});
      }
    } else {
      if(creep.harvest(sources[0]) === ERR_NOT_IN_RANGE) {
        creep.moveTo(sources[0], {visualizePathStyle: {stroke: '#ffaa00'}});
      }
    }
  }
};

export default roleMiner;
