const roleTower = {
  run(tower: any) {
    const roads: [StructureRoad] = tower.room.find(FIND_STRUCTURES, {
      filter: (structure: any) => {
        return structure.structureType === STRUCTURE_ROAD && (structure.hits / structure.hitsMax <= 0.75);
      }
    });

    const containers: [StructureContainer] = tower.room.find(FIND_STRUCTURES, {
      filter: (structure: any) => {
        return structure.structureType === STRUCTURE_CONTAINER && (structure.hits / structure.hitsMax <= 0.75);
      }
    });

    const storages: [StructureStorage] = tower.room.find(FIND_STRUCTURES, {
      filter: (structure: any) => {
        return structure.structureType === STRUCTURE_STORAGE && (structure.hits / structure.hitsMax <= 0.75);
      }
    });

    const ramparts: [StructureRampart] = tower.room.find(FIND_STRUCTURES, {
      filter: (structure: any) => {
        return (structure.structureType === STRUCTURE_RAMPART || structure.structureType === STRUCTURE_WALL) && (structure.hits < 35000);
      }
    });

    const invader: Creep = tower.pos.findClosestByPath(FIND_HOSTILE_CREEPS, {
      filter: (object: Creep) => {
        return object.getActiveBodyparts(ATTACK) !== 0;
      }
    });

    if (invader) {
      tower.attack(invader);
    } else if (roads.length > 0) {
      tower.repair(roads.sort((a: StructureRoad, b: StructureRoad) => {
        return a.hits === b.hits ? 0 : (a.hits < b.hits ? -1 : 1);
      })[0]);
    } else if (ramparts.length > 0) {
      tower.repair(ramparts.sort((a: StructureRampart, b: StructureRampart) => {
        return a.hits === b.hits ? 0 : (a.hits < b.hits ? -1 : 1);
      })[0]);
    } else if (storages.length > 0) {
      tower.heal(storages[0]);
    } else if (containers.length > 0) {
      tower.repair(containers[0]);
    }
  }
}

export default roleTower;
