import { ErrorMapper } from "utils/ErrorMapper";
import { Upgrader } from './role.upgrader';
import roleMiner from "./role.miner";
import roleTower from "./role.tower";

// When compiling TS to JS and bundling with rollup, the line numbers and file names in error messages change
// This utility uses source maps to get the line numbers and file names of the original, TS source code
export const loop = ErrorMapper.wrapLoop(() => {
  // tslint:disable-next-line:no-shadowed-variable
  for(const name in Memory.creeps) {
    if(!Game.creeps[name]) {
      delete Memory.creeps[name];
      console.log('Clearing non-existing creep memory:', name);
    }
  }

  if(Game.cpu.bucket === 10000) {
    Game.cpu.generatePixel();
  }

  const upgraders: Upgrader[] = _.filter(Game.creeps, (creep) => creep.memory.role === 'upgrader') as Upgrader[];
  const towerFeeders: Upgrader[] = _.filter(Game.creeps, (creep) => creep.memory.role === 'towerFeeder') as Upgrader[];
  const spawnFeeders: Upgrader[] = _.filter(Game.creeps, (creep) => creep.memory.role === 'spawnFeeder') as Upgrader[];
  const miners = _.filter(Game.creeps, (creep) => creep.memory.role === 'miner');
  const towers = Game.spawns['spawn_main'].room.find(FIND_STRUCTURES, {
    filter: (structure: any) => {
      return structure.structureType === STRUCTURE_TOWER;
    }
  });

  /* if(!Game.spawns['spawn_main'].room.controller!.safeMode && Game.spawns['spawn_main'].room.controller!.safeModeAvailable) {
      Game.spawns['spawn_main'].room.controller!.activateSafeMode();
  } */

  if(spawnFeeders.length < 3) {
    const newName = 'SpawnFeeders' + Game.time;
    const res = Game.spawns['spawn_main'].spawnCreep([WORK,WORK,CARRY,CARRY,CARRY,CARRY,MOVE,MOVE,MOVE], newName,
      {memory: {role: 'spawnFeeder', room: 'E5S12', working: true }});
    if (res !== 0 && spawnFeeders.length < 1) {
      Game.spawns['spawn_main'].spawnCreep([WORK,CARRY,CARRY,MOVE,MOVE], newName,
        {memory: {role: 'spawnFeeder', room: 'E5S12', working: true }});
    }
  } else if(miners.length < 2) {
    const newName = 'Miner' + Game.time;
    console.log('Spawning new miner: ' + newName);
    Game.spawns['spawn_main'].spawnCreep([WORK,WORK,WORK,WORK,WORK,MOVE,MOVE], newName,
      {memory: {role: 'miner', room: 'E5S12', working: true }});
  } else if(upgraders.length < 4) {
    const newName = 'Upgrader' + Game.time;
    console.log('Spawning new upgrader: ' + newName);
    Game.spawns['spawn_main'].spawnCreep([WORK,WORK,CARRY,CARRY,CARRY,CARRY,MOVE,MOVE,MOVE], newName,
      {memory: {role: 'upgrader', room: 'E5S12', working: true }});
  } else if(towerFeeders.length < 2) {
    const newName = 'TowerFeeder' + Game.time;
    console.log('Spawning new towerFeeder: ' + newName);
    Game.spawns['spawn_main'].spawnCreep([WORK,WORK,CARRY,CARRY,CARRY,CARRY,MOVE,MOVE,MOVE], newName,
      {memory: {role: 'towerFeeder', room: 'E5S12', working: true }});
  }

  if(Game.spawns['spawn_main'].spawning) {
    const spawningCreep = Game.creeps[Game.spawns['spawn_main'].spawning.name];
    Game.spawns['spawn_main'].room.visual.text(
      '🛠️' + spawningCreep.memory.role,
      Game.spawns['spawn_main'].pos.x + 1,
      Game.spawns['spawn_main'].pos.y,
      {align: 'left', opacity: 0.8});
  }

  for(const name in Game.creeps) {
    const creep = Game.creeps[name];
    if(creep.memory.role === 'upgrader' || creep.memory.role === 'spawnFeeder' || creep.memory.role === 'towerFeeder') {
      const upgrader = new Upgrader(creep);
      upgrader.run();
    }
    if(creep.memory.role === 'miner') {
      roleMiner.run(creep);
    }
  }

  for(const tower of towers) {
    roleTower.run(tower);
  }
});
